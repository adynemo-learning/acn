/**
 * Chapter 5
 */
/**
 * Readable streams
 * data, end, error, close,...
 */
// const createReadStream = require("fs").createReadStream;

// const readable = createReadStream("./fruits.txt");
// readable.on("data", (chunk) => {
//     console.log(chunk.toString());
// });

// readable.on("end", () => {
//     console.log("End!");
// });

/**
 * Writable streams
 * pipe, close error, drain,...
 */
// const { createWriteStream, createReadStream } = require("fs");

// const readStream = createReadStream("./fruits.txt");
// const writeStream = createWriteStream("./fruits_copy.txt");

// readStream.on("data", (chunk) => {
//     writeStream.write(chunk);
// });

// readStream.on("error", (err) => {
//     console.log(`error: ${err.message}`);
// });

// readStream.on("on", () => {
//     writeStream.end();
// });

/**
 * Write a huge file
 */
// const { createWriteStream } = require("fs");

// const writeStream = createWriteStream("./massif.txt");

// for (let i = 0; i <= 1e5; i++) {
//     writeStream.write(`${i} - je me répète mais je ne suis pas fou.\n`);
// }

// writeStream.end();

/**
 * Process stream
 */
// process.stdin.pipe(process.stdout);

// const { createReadStream } = require("fs");

// createReadStream("./fruits.txt").pipe(process.stdout);

/**
 * process.argv
 */
// const { createReadStream } = require("fs");

// createReadStream(process.argv[2]).pipe(process.stdout);

/**
 * Chapter 6
 */
/**
 * Custom readable extends Readable
 */
// const { Readable } = require("stream");

// const text = `Gros texte
// sur plusieurs lignes
// et c'est déjà la fin
// `;

// class StreamText extends Readable {
//     constructor(text) {
//         super();
//         this.text = text;
//         this.sentences = text.split("\n");
//     }

//     _read() {
//         this.sentences.map((data) => {
//             this.push(data);
//         });
//         this.push(null);
//     }
// }

// const streamText = new StreamText(text);

// streamText.on("data", (chunk) => console.log(chunk.toString()));
// streamText.on("end", () => console.log("end"));

/**
 * Custom writable extends Writable
 */
// const { Writable } = require("stream");

// class CustomWritable extends Writable {
//     _write(chunk, encoding, next) {
//         console.log(chunk.toString().trim().toUpperCase());
//         next();
//     }
// }

// const cw = new CustomWritable();
// streamText.pipe(cw);

/**
 * Back pressure
 */
// const { createReadStream, createWriteStream } = require("fs");

// const myReadStream = createReadStream("./massif.txt");
// const myWriteStream = createWriteStream("./massif_copy.txt", {
//     highWaterMark: 100000,
// });

// let nbOfPauses = 0;

// myReadStream.on("data", (chunk) => {
//     const isReadyToWriteMoreData = myWriteStream.write(chunk);

//     if (!isReadyToWriteMoreData) {
//         nbOfPauses++;
//         console.log("Trop de données poussées pour moi", nbOfPauses);
//         myReadStream.pause();
//     }
// });

// myWriteStream.on("drain", () => {
//     console.log("De nouveau prêt à écrire");
//     myReadStream.resume();
// });

/**
 * Write in file from terminal
 */
// const { createWriteStream } = require("fs");

// const writeStream = createWriteStream("./fruits.txt");

// process.stdin.pipe(writeStream);

/**
 * Chapter 7
 * Duplex streams
 */
/**
 * Transform stream
 */
// const { Transform } = require("stream");

// class Slugify extends Transform {
//     _transform(chunk, encoding, next) {
//         const slug = chunk.toString().trim().replace(/\s+/g, "-");
//         this.push(slug + "\n");
//         next();
//     }

//     _flush(next) {
//         console.log("bye bye");
//     }
// }

// const slugify = new Slugify();

// process.stdin.pipe(slugify).pipe(process.stdout);

/**
 * PassThrough stream
 */
// const { createReadStream, createWriteStream } = require("fs");
// const { PassThrough } = require("stream");

// // const myWriteStream = createWriteStream("./big.txt");

// // for (let i = 0; i <= 100; i++) {
// //     myWriteStream.write(`${i} - je me répète mais je ne suis pas fou.\n`);
// // }

// // myWriteStream.end();

// const myReadStream = createReadStream("./big.txt");
// const myPassThrough = new PassThrough();

// let total = 0;

// myPassThrough.on("data", (chunk) => {
//     total += chunk.length;
//     console.log(`${total} octets`);
// });

// myReadStream.pipe(myPassThrough).pipe(process.stdout);

/**
 * Duplex stream
 */
// const net = require("net");

// net.createServer(function (stream) {
//     stream.pipe(stream);
// }).listen(5000);

/**
 * Send data as object
 */
const { Readable } = require("stream");

const text = `Gros texte
sur plusieurs lignes
et c'est déjà la fin`;

class StreamText extends Readable {
    constructor(text) {
        super({ objectMode: true });
        this.text = text;
        this.sentences = text.split("\n");
    }

    _read() {
        this.sentences.map((data) => {
            const obj = {
                data: data,
                length: data.length,
            };
            this.push(obj);
        });
        this.push(null);
    }
}

const streamText = new StreamText(text);

streamText.on("data", (chunk) => console.log(JSON.stringify(chunk)));
streamText.on("end", () => console.log("end"));
