exports.getRandomNumber = (maxValue, callback) => {
    let error;
    if (typeof maxValue !== 'number') {
        error = new Error('Veuillez passer un nombre.');
    }
    const result = Math.floor(Math.random() * maxValue);
    if (callback !== undefined) {
        setTimeout(() => {
            callback(error, result);
        })
    }
    return new Promise((resolve, reject) => {
        if (typeof maxValue !== 'number') {
            reject(error);
        }
        resolve(result);
    });
}
