exports.getRandomNumber = (maxValue, callback) => {
    setTimeout(() => {
        if (typeof maxValue !== 'number') {
            return callback(new Error('Veuillez passer un nombre.'));
        }
        const result  = Math.floor(Math.random() * maxValue);
        callback(null, result);
    }, 2000);
};

exports.add = (firstNumber, secondNumber, callback) => {
    setTimeout(() => {
        const result = firstNumber + secondNumber;
        callback(null, result);
    }, 0);
}
