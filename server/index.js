/**
 * Chapter 7
 * Server without streams
 */
// const http = require("http");
// const fs = require("fs");
// const videoPath = "./videos/movie.mp4";

// const server = http.createServer();

// server.on("request", (req, res) => {
//     if (req.url === "/favicon") return;
//     if (req.url === "/contact") {
//         memoryUsageInMegaBytes(`route ${req.url}`);
//         res.end("contactez-nous");
//     } else if (req.url === "/videos") {
//         fs.readFile(videoPath, (error, data) => {
//             if (error) {
//                 console.error(`échec de la lecture : ${error.message}`);
//             }
//             memoryUsageInMegaBytes(`route ${req.url}`);
//             res.writeHead(200, { "Content-Type": "video/mp4" });
//             res.end(data);
//         });
//     } else {
//         res.end(`sur la page : ${req.url}`);
//     }
// });

// const port = 3000;
// server.listen(port, () => {
//     console.log(`serveur sur port ${port}`);
// });

// function memoryUsageInMegaBytes(pageUrl) {
//     const used = process.memoryUsage();

//     console.log(`====== ${pageUrl} start =====`);

//     for (let key in used) {
//         console.log(
//             `${key} ${Math.round((used[key] / 1024 / 1024) * 100) / 100} Mo`
//         );
//     }

//     console.log(`====== ${pageUrl} end =====`);
// }

/**
 * Chapter 8
 * Server with streams
 */
const http = require("http");
const { createReadStream } = require("fs");
const videoPath = "./videos/movie.mp4";

const server = http.createServer();

server.on("request", (req, res) => {
    if (req.url === "/favicon") return;
    if (req.url === "/contact") {
        memoryUsageInMegaBytes(`route ${req.url}`);
        res.end("contactez-nous");
    } else if (req.url === "/videos") {
        const myReadStream = createReadStream(videoPath);
        res.writeHead(200, { "Content-Type": "video/mp4" });
        myReadStream.pipe(res);
        memoryUsageInMegaBytes(`route ${req.url}`);
    } else {
        res.end(`sur la page : ${req.url}`);
    }
});

const port = 3000;
server.listen(port, () => {
    console.log(`serveur sur port ${port}`);
});

function memoryUsageInMegaBytes(pageUrl) {
    const used = process.memoryUsage();

    console.log(`====== ${pageUrl} start =====`);

    for (let key in used) {
        console.log(
            `${key} ${Math.round((used[key] / 1024 / 1024) * 100) / 100} Mo`
        );
    }

    console.log(`====== ${pageUrl} end =====`);
}
