/**
 * Sync & async
 * Chapter 1
 */
// const synchronous = require('./synchronicity/synchronous');
// const asynchronous = require('./synchronicity/asynchronous_1');
// const promises = require('./synchronicity/promises');

// promises.getRandomNumber(8, (err, res) => {
//     if (err) throw err;
//     console.log(`nombre généré : ${res}`);
// });

// // asynchronous.add(2, 5, (err, res) => {
// //     if (err) throw err;
// //     console.log(`result: ${res}`);
// // })

// promises
//     .getRandomNumber(10)
//         .then(data => console.log(data))
//         .catch(error => console.error(error.message));

// console.log('Hello World');

/**
 * EventEmitter
 * Chapter 2
 */
// const EventEmitter = require('events').EventEmitter;

// const programmer = new EventEmitter();

// programmer.on('drinkCoffee', () => {
//     console.log('coup de fouet');
// });

// const coffeeType = (coffee = 'café noir') => {
//     console.log(`type : ${coffee}`);
// }

// programmer.on('drinkCoffee', coffeeType);

// programmer.on('drinkCoffee', () => {
//     console.log('sans sucre');
// });

// programmer.emit('drinkCoffee', 'café brésilien');

/**
 * Custom class extended EventEmitter
 * Chapter 3
 */
// const ShoppingList = require('./events/ShoppingList');

// const myShoppingList = new ShoppingList();

// myShoppingList.on('added', (list) => {
//     console.log('Liste :', list);
// });

// myShoppingList.on('error', (err) => {
//     console.error(err.message);
// })

// myShoppingList.once('bringFreezerBag', (data) => {
//     console.log('sac de congélation prévu', data);
// });

// myShoppingList.add('camembert');
// myShoppingList.add('haricots verts surgelés');
// myShoppingList.add('eau minéral');
// myShoppingList.add('poissons surgelés');
// myShoppingList.add('steak hachés surgelés');
// myShoppingList.add('cocaïne');

/**
 * To return an EventEmitter
 * Chapter 4
 */
const EventEmitter = require("events").EventEmitter;

const getBook = () => {
    const ee = new EventEmitter();
    // process.nextTick or setImmediate
    process.nextTick(() => {
        ee.emit("searchBookStarted");
    });
    const searchDuration = Math.floor(Math.random() * 5 * 1000);
    setTimeout(() => {
        const book = { title: "Harry Potter", author: "J.K.Rowling" };
        ee.emit("bookFound", book);
    }, searchDuration);
    return ee;
};

const desiredBook = getBook();

desiredBook.on("searchBookStarted", () => {
    console.log("La recherche du livre a commencé.");
});

desiredBook.on("bookFound", (book) => {
    console.log(`Livre trouvé : ${JSON.stringify(book)}`);
});
